<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lote_model extends CI_Model
{
	// construtor
	function __construct()
	{
		parent:: __construct();
	}		
	
	// get codigo
	public function get_codigo($codigo)
	{
		$this->db->where('codigo',$codigo);
		$query = $this->db->get('lotes',1);
		if($query->num_rows() == 1)
		{
			return TRUE;
		}
		else
		{
			return NULL;
		}
	}
	
	// listar
	public function listar()
	{
		$sql = "SELECT * FROM lotes ORDER BY data";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		else
		{
			return NULL;
		}
	}
	
	// salvar (insert)
	public function salvar($dadosprod)
	{	
		// insert
		$this->db->insert('lotes',$dadosprod);
		return $this->db->insert_id();
	}	
	
	// excluir
	public function excluir($id)
	{
		$sql = "DELETE FROM lotes WHERE id = ".$id;				
		$query = $this->db->query($sql);
		return $this->db->affected_rows();
	}
	
}

