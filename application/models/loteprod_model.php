<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Loteprod_model extends CI_Model
{
	// construtor
	function __construct()
	{
		parent:: __construct();
	}		
	
	// listar
	public function listar()
	{
		$sql = "SELECT l.id AS id_lote, l.codigo AS cod_lote, l.data, p.id AS id_prod, p.codigo AS cod_prod, p.descricao, p.categoria, p.valor, SUM(lp.quantidade) AS quantidade FROM lote_produto lp 
				INNER JOIN produtos p ON p.id = lp.id_produto
				INNER JOIN lotes l ON l.id = lp.id_lote
				WHERE l.id = ".$_GET['id']."           
				GROUP BY lp.id_produto
				ORDER BY p.id";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		else
		{
			return NULL;
		}
	}
	
	// salvar (insert)
	public function salvar($dados)
	{	
		// insert
		$this->db->insert('lote_produto',$dados);
		return;
	}	
	
	// excluir
	public function excluir($id)
	{
		$sql = "DELETE FROM lote_produto WHERE id_lote = ".$id;				
		$query = $this->db->query($sql);
		return $this->db->affected_rows();
	}
	
}

