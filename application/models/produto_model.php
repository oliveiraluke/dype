<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produto_model extends CI_Model
{
	// construtor
	function __construct()
	{
		parent:: __construct();
	}	
	
	// get produto
	public function get_produto($id)
	{
		$this->db->where('id',$id);
		$query = $this->db->get('produtos',1);
		if($query->num_rows() == 1)
		{
			return $query->result();
		}
		else
		{
			return NULL;
		}
	}
	
	// get codigo
	public function get_codigo($codigo)
	{
		$this->db->where('codigo',$codigo);
		$query = $this->db->get('produtos',1);
		if($query->num_rows() == 1)
		{
			return TRUE;
		}
		else
		{
			return NULL;
		}
	}
	
	// listar
	public function listar()
	{
		$sql = "SELECT * FROM produtos ORDER BY id";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		else
		{
			return NULL;
		}
	}
							
	// salvar (insert/update)
	public function salvar($dados)
	{	
		if(isset($dados['id']) && $dados['id'] > 0)
		{
			// update
			$sql = "UPDATE produtos SET codigo='".$dados['codigo']."', descricao='".$dados['descricao']."', categoria='".$dados['categoria']."', valor='".$dados['valor']."' WHERE id=".$dados['id'];	
			$query = $this->db->query($sql);
			return $this->db->affected_rows();
		}
		else
		{
			// insert
			$this->db->insert('produtos',$dados);
			return $this->db->insert_id();
		}
	}
	
}

