<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lotes extends CI_Controller 
{
	// construtor
	function __construct()
	{
		parent:: __construct();
		$this->load->model('lote_model','lote');
		$this->load->model('loteprod_model','loteprod');
		$this->load->model('produto_model','produto');
	}
	
	// index
	public function index()
	{			
		$dados['titulo'] = 'Mercearia - controle de estoque';		
		$dados['pagina'] = 'Lotes';
		$dados['produtos'] = $this->loteprod->listar();
		$this->load->view('lotes', $dados);
	}
	
	// adiciona
	public function adiciona()
	{			
		$dados['titulo'] = 'Mercearia - controle de estoque';		
		$dados['pagina'] = 'Novo lote';
		$dados['produto'] = $this->produto->listar();
		$this->load->view('formlote', $dados);
	}
			
	// salvar
	public function salvar()
	{				
		$dados['codigo'] = $_POST['codigo'];
		$dados['data'] = date('Y-m-d');
		
		// salvar
		if($this->lote->get_codigo($dados['codigo']) == true)
		{
			$aux = 3;
		}
		else
		{
			$id = $this->lote->salvar($dados);
			
			// salva produtos
			$dadosprod['id_lote'] = $id;	
			foreach($_POST['produto'] as $k => $v)
			{							
				$dadosprod['id_produto'] = $v;				
				$dadosprod['quantidade'] = $_POST['quantidade'][$k];					
				$this->loteprod->salvar($dadosprod);
			}
			$aux = 1;
		}
			
		// retorno
		$info = array
		(
			'status' => $aux,
		);			
		echo json_encode($info);		
	}
	
	// excluir
	public function excluir()
	{				
		$id = $_POST["id"];
		if(($this->lote->excluir($id)) && ($this->loteprod->excluir($id)))
		{
			// retorno
			$info = array
			(
				'status' => 2,
			);				
			echo json_encode($info);
		}
	}
		
}
