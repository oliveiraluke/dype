<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller 
{
	// construtor
	function __construct()
	{
		parent:: __construct();
		$this->load->model('lote_model','lote');
		$this->load->model('produto_model','produto');
	}
	
	// index (main)
	public function index()
	{			
		$dados['titulo'] = 'Mercearia - controle de estoque';		
		$dados['pagina'] = 'Controle de Estoque';		
		$dados['lotes'] = $this->lote->listar();
		$dados['produtos'] = $this->produto->listar();
		$this->load->view('main', $dados);
	}
}
