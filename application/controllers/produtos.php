<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produtos extends CI_Controller 
{
	// construtor
	function __construct()
	{
		parent:: __construct();
		$this->load->model('produto_model','produto');
	}
	
	// index
	public function index()
	{			
	}
	
	// adiciona
	public function adiciona()
	{			
		$dados['titulo'] = 'Mercearia - controle de estoque';		
		$dados['pagina'] = 'Novo produto';
		$this->load->view('formproduto', $dados);
	}	
	
	// editar
	public function editar()
	{			
		$dados['titulo'] = 'Mercearia - controle de estoque';		
		$dados['pagina'] = 'Edita produto';		
		$dados['produto'] = $this->produto->get_produto($_GET['id']);
		$this->load->view('formproduto', $dados);
	}	
			
	// salvar/atualizar
	public function salvar()
	{				
		$dados['id'] = $_POST['id'];
		$dados['codigo'] = $_POST['codigo'];
		$dados['descricao'] = $_POST['descricao'];
		$dados['categoria'] = $_POST['categoria'];
		
		// converte moeda para número
		$atual = array("R$ ", ".", ",");
		$altera = array("", "", ".");
		$dados['valor'] = str_replace($atual,$altera,$_POST['valor']);
		
		//atualizar
		if($dados['id'])
		{
			if($this->produto->salvar($dados))
			{
				$aux = 2;
			}
		}
		// salvar
		else
		{
			if($this->produto->get_codigo($dados['codigo']) == true)
			{
				$aux = 3;
			}
			else
			{
				if($this->produto->salvar($dados))
				{
					$aux = 1;
				}
			}
		}
				
		// retorno
		$info = array
		(
			'status' => $aux,
		);			
		echo json_encode($info);
	}
		
}
