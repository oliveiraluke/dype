<!-- Header -->
<?php $this->load->view('header'); ?>
			
	<!-- Main -->
	<section class="window">
		<div class="container-fluid">	
				
			<!-- Edição -->
			<div class="row" id="edicao">	
				
				<!-- Form lotes -->
				<div id="divLotes">	
				
					<!-- Formulário -->
					<form method="" action="" class="form-horizontal col-md-12" name="formInfo" id="formInfo">	
											
						<!-- Código -->
						<div class="col-sm-6">
							<label for="codigo">Código:</label>
							<input type="text" class="form-control campoNumero" id="codigo" name="codigo" placeholder="Código" value="" maxlength="8" />
						</div>	
						
						<!-- Data de cadastro -->
						<div class="col-sm-6">
							<label for="data">Data de cadastro:</label>
							<input type="text" class="form-control campoData" id="data" name="data" readonly value="<?php echo date("d/m/Y"); ?>" />
						</div>
						
						<!-- Adiciona produto -->
						<div id="boxprod" class="prod">	
						
							<!-- Produto -->
							<div class="col-sm-6">
								<label for="produto">Produto:</label>
								<select class="form-control" id="produto" name="produto[]" placeholder="Produto">
									<option selected value="">Selecione um produto</option><?php
										if(isset($produto) && (sizeof($produto) > 0))
										{
											foreach($produto as $produto)
											{?>
												<option value="<?php if(!empty($produto)){ echo $produto->id; } ?>"><?php if(!empty($produto)){ echo $produto->codigo." - ".$produto->descricao; } ?></option><?php
											}
										}?>	
								</select>								
							</div>
						
							<!-- Quantidade -->
							<div class="col-sm-6">
								<label for="quantidade">Quantidade:</label>
								<input type="text" class="form-control campoNumero" id="quantidade" name="quantidade[]" placeholder="Quantidade" value="" />
							</div>
						
						</div>
						
						<!-- Mais produtos -->
						<div class="maisprod prod">
						</div>
						
						<!-- Mais itens -->
						<div class="col-sm-12">	
							<button type="button" class="btn btn-primary btn-mais" title="Adicionar produto"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Itens</button>
						</div>	
						
					</form>	
						
					<!-- Informações -->
					<div class="col-sm-12 obs">
						<p class="text-muted pull-right"><i class="fa fa-exclamation-circle" aria-hidden="true" title="informação"></i>&nbsp;Todos os campos são obrigatórios.</p>
					</div>
						
				</div>
			
				<!-- Botões -->
				<div class="botoes">
					<div class="col-sm-12">	
							
						<!-- Funções -->
						<div class="pull-right">
							<button type="button" class="btn btn-danger btn-cancelar" title="Cancelar"><i class="fa fa-times" aria-hidden="true"></i>&nbsp;Cancelar</button>
							<button type="button" id="slote" class="btn btn-success btn-salvar" title="Salvar"><i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Salvar</button>
						</div>
						
					</div>								
				</div>	
				
			</div>			
			
		</div>		
	</section>
			
	</body>
</html>