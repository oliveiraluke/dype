<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 	
<html lang="pt-br">  
	<head>  
  
		<!-- Meta informações -->
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" >
		<meta name="viewport" content="width=device-width, initial-scale=1.0">  
		<meta name="description" content="Mercearia - controle de estoque">
		<meta name="keywords" content="mercearia, controle, estoque">
		
		<!-- Favicon -->	
		<link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.png') ?>" type="image/x-icon"/> 
		
		<!-- Title -->
		<title><?php echo $titulo; ?></title>
			
		<!-- CSS -->	    
		<link href="<?php echo base_url('assets/css/bootstrap/bootstrap.min.css') ?>" rel="stylesheet" type="text/css">  		
		<link href="<?php echo base_url('assets/css/bootstrap/bootstrap-theme.min.css') ?>" rel="stylesheet" type="text/css"> 
		<link href="<?php echo base_url('assets/css/font-awesome/font-awesome.min.css') ?>" rel="stylesheet" type="text/css">			
		<link href="<?php echo base_url('assets/css/sweetalert.min.css') ?>" rel="stylesheet" type="text/css">			
		<link href="<?php echo base_url('assets/css/estilo.css') ?>" rel="stylesheet" type="text/css">			
			
		<!-- Font -->	
		<link href="http://fonts.googleapis.com/css?family=Maven+Pro" rel="stylesheet" type="text/css">
					
		<!-- JavaScript -->	
		<script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
		<script src="<?php echo base_url('assets/js/bootstrap/bootstrap.min.js') ?>" type="text/javascript"></script>
		<script src="<?php echo base_url('assets/js/maskedinput.min.js') ?>" type="text/javascript"></script>
		<script src="<?php echo base_url('assets/js/maskmoney.js') ?>" type="text/javascript"></script>
		<script src="<?php echo base_url('assets/js/sweetalert.min.js') ?>" type="text/javascript"></script>
		<script src="<?php echo base_url('assets/js/forms.js') ?>" type="text/javascript"></script>
		<script src="<?php echo base_url('assets/js/lateral.js') ?>" type="text/javascript"></script>
		<script src="<?php echo base_url('assets/js/principal.js') ?>" type="text/javascript"></script>
				
	</head>
	<body>
			
		<!-- Header -->
		<header>
		</header>
	