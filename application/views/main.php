<!-- Header -->
<?php $this->load->view('header'); ?>
	
	<!-- Menu -->
	<?php $this->load->view('menu'); ?>
		
		<!-- Cabeçalho -->
		<section class="section" id="cabecalho">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">	
						<center><h2><?php echo $pagina; ?></h2></center>
					</div>	
				</div>				
			</div>
		</section>
			
		<!-- Main -->
		<section class="section main">
			<div class="container-fluid">
				
				<!-- Conteúdo -->
				<div class="row">
					<div class="col-md-10 col-md-offset-1">
					
						<!-- Barra Lateral -->
						<div class="col-md-3 lateral">
							
							<!-- Menu lateral -->
							<div class="row" id="menulateral">
								<div class="col-md-12">
									<div class="panel panel-default">
										<div class="panel-heading">
											Gerenciar
										</div>
										<div class="panel-body">
											<div class="collapse in left-menu">											
												<div class="filter-list list-group">
													
													<!-- lotes -->
													<a href="#" id="btn-lotes" class="list-group-item">
														<i class="fa fa-archive fa-fw" aria-hidden="true" title="Lotes"></i>&nbsp;&nbsp;Lotes
													</a>
													
													<!-- produtos -->
													<a href="#" id="btn-produtos" class="list-group-item">
														<i class="fa fa-file-text fa-fw" aria-hidden="true" title="Produtos"></i>&nbsp;&nbsp;Produtos
													</a>		
										
												</div>
											</div>
										</div>
									</div>								
								</div>
							</div>
							
						</div>
						
						<!-- Principal -->
						<div class="col-md-9 principal">		
																
							<!-- Lotes -->
							<div id="lotes" class="row">
								<div class="col-md-12">
									<div class="panel panel-default">
										<div class="panel-heading">
											Lotes
										</div>
										<div class="panel-body"><?php	
										
											# lotes
											if(isset($lotes) && (sizeof($lotes) > 0))
											{
												# tabela ?>
												<table class="table-condensed table-striped col-md-12" border="0" cellpadding="0" cellspacing="0">
													<thead>
														<tr>
															<td><strong>#</strong></td>
															<td><strong>código</strong></td>
															<td><strong>data de cadastro</strong></td>
															<td><strong>ações</strong></td>
														</tr>
													</thead>
													<tbody><?php		
														$cont=1;
														foreach($lotes as $lote)
														{?>
															<tr>
																<td><?php echo $cont; ?></td>
																<td><?php echo $lote->codigo; ?></td>
																<td><?php echo date('d/m/Y', strtotime($lote->data)); ?></td>
																<td>
																	<button class="btn btn-primary btn-sm btn-viewl" name="Visualizar" title="Visualizar" value="<?php echo $lote->id.'-'.$lote->codigo; ?>">
																		<i class="fa fa-eye" aria-hidden="true"></i>
																	</button>
																	<button class="btn btn-danger btn-sm btn-dell" name="Excluir" title="Excluir" value="<?php echo $lote->id; ?>">
																		<i class="fa fa-trash-o" aria-hidden="true"></i>
																	</button>
																</td>
															</tr><?php
															$cont++;
														}?>
													</tbody>
												</table><?php
											}
											else
											{
												# info ?>
												<div class="alert alert-info alert-dismissible col-md-12" role="alert">
													<button type="button" class="close" data-dismiss="alert" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
													Nenhum lote cadastrado. Clique em <strong>Cadastrar</strong> para adicionar um novo lote de mercadorias.
												</div> <?php
											}?>

											<!-- botões -->
											<div class="pull-right botoes">
												<button class="btn btn-success btn-sm btn-addl" name="Cadastrar" title="Cadastrar">
													<i class="fa fa-plus" aria-hidden="true"></i>
												</button>
											</div>								
										
										</div>
									</div>
								</div>
							</div>
															
							<!-- Produtos -->
							<div class="row" id="produtos">
								<div class="col-md-12">
									<div class="panel panel-default">
										<div class="panel-heading">
											Produtos
										</div>
										<div class="panel-body"><?php			
											# produtos
											if(isset($produtos) && (sizeof($produtos) > 0))
											{
												# tabela ?>
												<table class="table-condensed table-striped col-md-12" border="0" cellpadding="0" cellspacing="0">
													<thead>
														<tr>
															<td><strong>#</strong></td>
															<td><strong>código</strong></td>
															<td><strong>descrição</strong></td>
															<td><strong>categoria</strong></td>
															<td><strong>valor</strong></td>
															<td><strong>ações</strong></td>
														</tr>
													</thead>
													<tbody><?php		
														$cont=1;
														foreach($produtos as $produto)
														{?>	
															<tr>
																<td><?php echo $cont; ?></td>
																<td><?php echo $produto->codigo; ?></td>
																<td><?php echo $produto->descricao; ?></td>
																<td><?php echo $produto->categoria; ?></td>
																<td><?php echo "R$ ".number_format($produto->valor, 2, ',', '.'); ?></td>
																<td>
																	<button class="btn btn-warning btn-sm btn-editp" name="Editar" title="Editar" value="<?php echo $produto->id; ?>">
																		<i class="fa fa-pencil" aria-hidden="true"></i>
																	</button>
																</td>
															</tr><?php
															$cont++;
														}?>
													</tbody>
												</table><?php
											}
											else											
											{		
												# info ?>
												<div class="alert alert-info alert-dismissible col-md-12" role="alert">
													<button type="button" class="close" data-dismiss="alert" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
													Nenhum produto cadastrado. Clique em <strong>Cadastrar</strong> ou <strong>Importar</strong> para adicionar um novo produto.
												</div> <?php
											} ?>

											<!-- botões -->
											<div class="pull-right botoes">												
												<button class="btn btn-success btn-sm btn-addp" name="Cadastrar" title="Cadastrar">
													<i class="fa fa-plus" aria-hidden="true"></i>
												</button>
												<button class="btn btn-success btn-sm dev" name="Importar XML" title="Importar XML">
													<i class="fa fa-upload" aria-hidden="true"></i>
												</button>										
												<button class="btn btn-success btn-sm dev" name="Exportar XML" title="Exportar XML">
													<i class="fa fa-download" aria-hidden="true"></i>
												</button>
												<button class="btn btn-success btn-sm dev" name="Relatório XML" title="Relatório XML">
													<i class="fa fa-file-archive-o" aria-hidden="true"></i>
												</button>
											</div>	
											
										</div>
									</div>
								</div>
							</div>
						
						</div>	
						
					</div>
				</div>
				
			</div>
		</section>
				
		<!-- Modal -->
		<?php $this->load->view('modal'); ?>
		
		<!-- Footer -->
		<?php $this->load->view('footer'); ?>
	
  </body>
</html>