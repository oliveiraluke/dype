<!-- Menu  -->
<nav class="navbar navbar-default navbar-fixed-top" id="menu" role="navigation">
	<div class="container-fluid col-md-10 col-md-offset-1">
				
		<!-- Cabeçalho do menu -->
		<div class="navbar-header">
				  
			<!-- Botão de menu -->
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
						
			<!-- Título do site -->	
			<a class="navbar-brand" href="#" alt="Mercearia - controle de estoque" title="Mercearia - controle de estoque">Mercearia</a>
					
		</div>

		<!-- Opções do menu -->
		<div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav navbar-right">
				<li><a href="#">Home</a></li>
			</ul>
		</div>
					
	</div>
</nav>