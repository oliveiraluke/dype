<!-- Header -->
<?php $this->load->view('header'); ?>
			
	<!-- Main -->
	<section class="window">
		<div class="container-fluid">	
				
			<!-- Edição -->
			<div class="row" id="edicao">	
				
				<!-- Form produtos -->
				<div id="divProd">	
				
					<!-- Formulário -->
					<form method="" action="" class="form-horizontal col-md-12" name="formInfo" id="formInfo">
					
						<?php
							if(isset($produto) && (sizeof($produto) > 0))
							{
								foreach($produto as $produto)
								{
								}
							}
						?>							
						
						<!-- Id -->
						<input type="hidden" id="id" name="id" value="<?php if(!empty($produto)){ echo $produto->id; } ?>" />						
						
						<!-- Código -->
						<div class="col-sm-6">
							<label for="codigo">Código:</label>
							<input type="text" class="form-control" id="codigo" name="codigo" placeholder="Código" value="<?php if(!empty($produto)){ echo $produto->codigo; } ?>" maxlength="13" />
						</div>
																				
						<!-- Descrição -->
						<div class="col-sm-6">
							<label for="descricao">Descrição:</label>
							<input type="text" class="form-control" id="descricao" name="descricao" placeholder="Descrição" value="<?php if(!empty($produto)){ echo $produto->descricao; } ?>" />
						</div>
																				
						<!-- Categoria -->
						<div class="col-sm-6">
							<label for="categoria">Categoria:</label>
							<select class="form-control" id="categoria" name="categoria" placeholder="Categoria">
								<option selected value="<?php if(!empty($produto)){ echo $produto->categoria; } ?>"><?php if(!empty($produto)){ echo $produto->categoria; } else { echo "Selecione uma categoria"; } ?></option>									
								<option value="Açúcares e doces">Açúcares e doces</option>
								<option value="Bebidas">Bebidas</option>
								<option value="Carnes e ovos">Carnes e ovos</option>
								<option value="Cereais, pães e massas">Cereais, pães e massas</option>
								<option value="Frutas">Frutas</option>
								<option value="Hortaliças">Hortaliças</option>
								<option value="Laticínios‎">Laticínios‎</option>
								<option value="Leguminosas">Leguminosas</option>
								<option value="Óleos e gorduras">Óleos e gorduras</option>
							</select>								
						</div>
												
						<!-- Valor -->
						<div class="col-sm-6">
							<label for="valor">Valor:</label>
							<input type="text" class="form-control campoReais" id="valor" name="valor" placeholder="Valor" value="<?php if(!empty($produto)){ echo 'R$ '.number_format($produto->valor, 2, ',', '.'); } ?>" />
						</div>	
						
					</form>	
						
					<!-- Informações -->
					<div class="col-sm-12 obs">
						<p class="text-muted pull-right"><i class="fa fa-exclamation-circle" aria-hidden="true" title="informação"></i>&nbsp;Todos os campos são obrigatórios.</p>
					</div>
						
				</div>
			
				<!-- Botões -->
				<div class="botoes">
					<div class="col-sm-12">	
							
						<!-- Funções -->
						<div class="pull-right">
							<button type="button" class="btn btn-danger btn-cancelar" title="Cancelar"><i class="fa fa-times" aria-hidden="true"></i>&nbsp;Cancelar</button>
							<button type="button" id="sprod" class="btn btn-success btn-salvar" title="Salvar"><i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Salvar</button>
						</div>
						
					</div>								
				</div>	
				
			</div>			
			
		</div>		
	</section>
			
	</body>
</html>