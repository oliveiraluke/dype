<!-- Header -->
<?php $this->load->view('header'); ?>
			
	<!-- Main -->
	<section class="window">
		<div class="container-fluid">	
				
			<!-- Edição -->
			<div class="row" id="edicao">
				<div class="col-md-12">					
												
					<!-- Lotes Produtos -->
					<div id="divLotes">	
						<div class="col-sm-12">	<?php	
							if(isset($produtos) && (sizeof($produtos) > 0))
							{?>
								<table class="table table-striped">
									<thead>
										<tr>
											<td><strong>código</strong></td>
											<td><strong>descrição</strong></td>
											<td><strong>categoria</strong></td>
											<td><strong>valor</strong></td>
											<td><strong>quantidade</strong></td>
										</tr>
									</thead>
									<tbody><?php		
										foreach($produtos as $produto)
										{?>
											<tr>
												<td><?php echo $produto->cod_prod; ?></td>
												<td><?php echo $produto->descricao; ?></td>
												<td><?php echo $produto->categoria; ?></td>
												<td><?php echo $produto->valor; ?></td>
												<td><?php echo $produto->quantidade; ?></td>													
											</tr><?php
										}?>
									</tbody>
								</table><?php
							}
							else
							{
								# info ?>
								<div class="alert alert-info alert-dismissible col-md-12" role="alert">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									Nenhum lote cadastrado. Clique em <strong>Cadastrar</strong> para adicionar um novo lote de mercadorias.
								</div><?php
							}?>
						</div>								
					</div>	
						
				</div>	
			</div>				
			
		</div>		
	</section>
			
	</body>
</html>