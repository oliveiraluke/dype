/*
	forms.js 	
		
	* máscaras de campos de formulários 
	* tratamento/validação de campos 
	* envia valores via Ajax 
	* exibe mensagens para o usuário
*/

$(function formularios()
{		
	// máscaras
	$(".campoReais").maskMoney({symbol:'R$ ', showSymbol:true, thousands:'.', decimal:',', symbolStay: true});
	$(".campoData").mask("99/99/9999");
	
	// validação número
	$('.campoNumero').on('change', function()
	{
		if((isNaN($(this).val())) && ($(this).length))
		{
			swal(
			{
				title: "Não é um número válido",
				text: "Tente novamente",
				type: 'warning',
				confirmButtonText: "Ok",
				allowOutsideClick: false
			});
			$(this).val('');
		}
	});
		
	// chama funções de acordo com os botões
	{
		$(".btn-cancelar").on('click', cancelar);
		$(".btn-salvar").on('click', salvar);
		$(".btn-mais").on('click', additem);
	}
		
	// adiciona item
	function additem()
	{			
		e=$("#boxprod").clone().appendTo(".maisprod");

        for(var i = 0; i < e.find('input,textarea').length; i++) 
		{
            e.find('input,textarea').eq(i).val('')
        };
	};	
	
	// cancelar
	function cancelar()
	{	
		parent.$(".modal").hide();
		parent.window.location.reload();	
	};	
	
	// salvar/atualiza
	function salvar()
	{	
		switch($(this).attr('id'))
		{
			case "sprod":
				// verifica campos obrigatórios
				if(	($("#codigo").val()!='') && (($("#codigo").val() || '').length == 13) && 
					($("#descricao").val()!='') && 
					($("#categoria").val()!='') && 
					($("#valor").val()!='') && ($("#valor").val()!='0') ) 	
				{
					// monta dados
					$pagina = 'http://localhost/dype/produtos/salvar';
					$dados = 'id=' + $("#id").val()
							+ '&codigo=' + $("#codigo").val() 
							+ '&descricao=' + $("#descricao").val()
							+ '&categoria=' + $("#categoria").val()
							+ '&valor=' + $("#valor").val();
							
					// envia dados		
					envia($dados,$pagina);
				}
				else
				{
					mensagem(0);
				}
			break;
			
			case "slote":
				// verifica campos obrigatórios
				if(	($("#codigo").val()!='') && (($("#codigo").val() || '').length == 8) && 
					($("#data").val()!='') && 
					($("#produto").val()!='') && 
					($("#quantidade").val()!='') && ($("#quantidade").val()!='0') ) 	
				{
					// monta dados
					{
						$pagina = 'http://localhost/dype/lotes/salvar';
					
						// produtos
						$produtos = '';
						var selects = document.getElementsByTagName('select');					
						for(var i=0; i < selects.length; i++)
						{
							if(selects[i].name=='produto[]')
							{
								$produtos +='&'+selects[i].name+'='+selects[i].value;
							}
						}	
						
						// quantidades
						$quantidades = '';
						var inputs = document.getElementsByTagName('input');				
						for(var i=0; i < inputs.length; i++)
						{
							if(inputs[i].name=='quantidade[]')
							{
								$quantidades +='&'+inputs[i].name+'='+inputs[i].value;
							}
						}	

						$dados = 'codigo=' + $("#codigo").val() 
								+ '&produto=' + $("#produto").val()
								+ '&quantidade=' + $("#quantidade").val()
								+ $produtos
								+ $quantidades;
					}
							
					// envia dados		
					envia($dados,$pagina);
				}
				else
				{
					mensagem(0);
				}
			break;
		}
	}
	
	// envia dados
	function envia($dados,$pagina)
	{
		$.ajax(
		{
			type      : 'post',
			url       : $pagina,
			data      : $dados, 
			dataType  : 'json',				 
			success : function(json)
			{
				mensagem(json.status);
			}
		});
	};
	
	// exibe mensagem para usuário
	function mensagem($status)
	{
		switch($status)
		{
			// campos obrigatórios
			case 0:
				swal(
				{
					title: "Preencha todos os campos obrigatórios",
					text: "Tente novamente",
					type: 'error',
					confirmButtonText: "Ok",
					allowOutsideClick: false
				}); 
			break;
			
			// informações salvas com sucesso			
			case 1:
				swal(
				{
					title: "Informações salvas com sucesso",     
					text: "", 
					type: "success",
					confirmButtonText: "Ok",
					allowOutsideClick: false
				})
				.then(
				function(isConfirm)
				{   
					if (isConfirm) 
					{    	
						parent.$(".modal").hide();
						parent.window.location.reload();
					} 
				}); 
			break;
			
			// informações atualizadas com sucesso			
			case 2:
				swal(
				{
					title: "Informações atualizadas com sucesso",     
					text: "", 
					type: "success",
					confirmButtonText: "Ok",
					allowOutsideClick: false
				})
				.then(
				function(isConfirm)
				{   
					if (isConfirm) 
					{    	
						parent.$(".modal").hide();
						parent.window.location.reload();
					} 
				}); 
			break;		

			// código existente
			case 3:
				swal(
				{
					title: "Código existente",
					text: "Tente novamente",
					type: 'error',
					confirmButtonText: "Ok",
					allowOutsideClick: false
				}); 
			break;			
		}
	};	
		
});