/*
	principal.js 	
		
	* monta modais dinamicamente conforme chamada
	* exibe páginas através de iframe
*/

$(function principal()
{		
	// desabilita href 
	$("#principal").find('a').removeAttr("href");
		
	// chama funções de acordo com os botões
	{
		$(".btn-viewl").on('click', visualizalote);
		$(".btn-dell").on('click', deletalote);
		$(".btn-addl").on('click', adicionalote);
		$(".btn-editp").on('click', editaprod);
		$(".btn-addp").on('click', adicionaprod);
		$(".dev").on('click', alertadev);
	}
		
	// visualiza lote
	function visualizalote()
	{
		$id = $(this).val().split('-')[0];
		$codigo = $(this).val().split('-')[1];		
		$("#modal").modal({show: true});
		$(".modal-title").empty().append('<i class="fa fa-archive fa-fw" aria-hidden="true" title="Lote&nbsp;'+$codigo+'"></i>&nbsp;Lote&nbsp;'+$codigo);
		$(".modal-body").empty().append('<iframe src="http://localhost/dype/lotes?id='+$id+'" scrolling="yes"></iframe>');
	};
			
	// adiciona lote
	function adicionalote()
	{		
		$("#modal").modal({show: true});
		$(".modal-title").empty().append('<i class="fa fa-archive fa-fw" aria-hidden="true" title="Novo lote"></i>&nbsp;Novo lote');
		$(".modal-body").empty().append('<iframe src="http://localhost/dype/lotes/adiciona" scrolling="yes"></iframe>');
	};	
	
	// adiciona produto
	function adicionaprod()
	{		
		$("#modal").modal({show: true});
		$(".modal-title").empty().append('<i class="fa fa-file-text fa-fw" aria-hidden="true" title="Novo produto"></i>&nbsp;Novo produto');
		$(".modal-body").empty().append('<iframe src="http://localhost/dype/produtos/adiciona" scrolling="yes"></iframe>');
	};
			
	// edita produto
	function editaprod()
	{
		$id = $(this).val();
		mensagem(0, function($resposta) 
		{
			if($resposta == true)
			{
				swal.close();
				$("#modal").modal({show: true});
				$(".modal-title").empty().append('<i class="fa fa-file-text fa-fw" aria-hidden="true" title="Edita produto"></i>&nbsp;Edita produto');
				$(".modal-body").empty().append('<iframe src="http://localhost/dype/produtos/editar?id='+$id+'" scrolling="yes"></iframe>');
			}
		});				
	};
	
	// deleta lote
	function deletalote()
	{
		$id = $(this).val();
		mensagem(1, function($resposta) 
		{
			if($resposta == true)
			{
				$pagina = 'http://localhost/dype/lotes/excluir';
				$dados = 'id=' + $id;	
				envia($dados,$pagina);
			}
		});			
	};
	
	// alerta recurso em desenvolvimento
	function alertadev()
	{
		mensagem(3);
	};
	
	// envia dados
	function envia($dados,$pagina)
	{
		$.ajax(
		{
			type      : 'post',
			url       : $pagina,
			data      : $dados, 
			dataType  : 'json',				 
			success : function(json)
			{
				mensagem(json.status);
			}
		});
	};
	
	// exibe mensagem para usuário
	function mensagem($status, callback)
	{
		switch($status)
		{						
			// confirmação de alteração - produto			
			case 0:
				swal(
				{
					title: "Deseja realmente editar este produto?", 	
					showCancelButton: true,     
					confirmButtonText: "Sim",
					cancelButtonText: "Não",
					closeOnConfirm: false, 
					allowOutsideClick: false
				})
				.then(
				function(isConfirm)
				{   
					if (isConfirm) 
					{ 
						callback(true);
					}
					else	
					{
						callback(false);
					}
				});		
			break;
			
			// confirmação de exclusão - lote
			case 1:
				swal(
				{
					title: "Deseja realmente excluir este lote?", 	
					showCancelButton: true,     
					confirmButtonText: "Sim",
					cancelButtonText: "Não",
					closeOnConfirm: false, 
					allowOutsideClick: false
				})
				.then(
				function(isConfirm)
				{   
					if (isConfirm) 
					{ 
						callback(true);
					}
					else	
					{
						callback(false);
					}
				});		
			break;
			
			// lote excluido com sucesso	
			case 2:
				swal(
				{
					title: "Lote excluido com sucesso",     
					text: "", 
					type: "success",
					confirmButtonText: "Ok",
					allowOutsideClick: false
				})
				.then(
				function(isConfirm)
				{   
					if (isConfirm) 
					{    	
						parent.$(".modal").hide();
						parent.window.location.reload();
					} 
				}); 
			break;	

			// recurso em desenvolvimento
			case 3:			
				swal(
				{
					title: "Recurso em desenvolvimento",     
					text: "", 
					type: "info",
					confirmButtonText: "Ok",
					allowOutsideClick: false
				})
				.then(
				function(isConfirm)
				{   
				});	
			break;	
		}
	};	
		
	// altera tipo de cursor
	$("#principal").find('a').on('mouseenter', function() 
	{
		$(this).css( 'cursor', 'pointer' );
	});
	
});