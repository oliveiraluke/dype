/*
	lateral.js 	
	
	* funções do menu (barra lateral)
*/

$(function menu()
{
	
	// ações durante carregamento da página
	$(document).ready(function()
	{			
		$("#menulateral").find('a').removeAttr("href");	
		$('#produtos').hide();		
	});	
		
	// chama funções de acordo com os botões
	{
		$("#btn-lotes").on('click', lotes);
		$("#btn-produtos").on('click', produtos);
	}	
	
	// lotes
	function lotes()
	{
		$("#produtos").hide("slow");
		$("#lotes").show("slow");			
	};
	
	// produtos
	function produtos()
	{		
		$("#lotes").hide("slow");
		$("#produtos").show("slow"); 	
	};
			
	// altera tipo de cursor
	$("#menulateral").find('a').on('mouseenter', function() 
	{
		$(this).css( 'cursor', 'pointer' );
	});
	
});