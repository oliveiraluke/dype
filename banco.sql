-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 13-Fev-2017 às 17:55
-- Versão do servidor: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aerovale`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `textos`
--

CREATE TABLE `textos` (
  `id` int(11) NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `posicao` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `textos`
--

INSERT INTO `textos` (`id`, `texto`, `posicao`, `status`) VALUES
(1, '<p>No coração do pólo aeronáutico brasileiro, está localizado um empreendimento feito especialmente para quem quer ir longe. O Aerovale é um condominio fechado, a apenas 4 km da Rod. Presidente Dutra, e na margem da Rod. Carvalho Pinto, em que empresas poderão contar com uma estrutura única na América Latina: uma pista de pouso de 1550 metros. Além disso, lotes com acesso direto a pista de pouso, permitem a hangaragem de aeronaves, manutenção, fabricação e muito mais. Um projeto pioneiro no país com capacidade para até 1500 aeronaves.</p>', 4, 1),
(2, '<h4>305 LOTES A PARTIR DE 722 M² COM ESTRUTURA COMPLETA</h4>\r\n\r\n<ul>\r\n<li>Pista de pouso</li>\r\n<li>Pátio para aeronaves</li>\r\n<li>Estacionamento exclusivo</li>\r\n<li>Segurança 24h</li>\r\n<li>Monitoramento eletrônico</li>\r\n<li>Rede de dados em fibra ótica</li>\r\n<li>Hotel e centro de convenções</li>\r\n<li>Área comecial e bancos</li>\r\n</ul>', 1, 1),
(3, '<h4>TOTAL SEGURANÇA</h4>\r\n\r\n<ul>\r\n<li>Segurança patrimonial 24h</li>\r\n<li>Rondas</li>\r\n<li>Vigilância eletrônica</li>\r\n<li>Controle de acesso</li>\r\n</ul>\r\n\r\n<h4>NÚMERO DE LOTES</h4>\r\n\r\n<ul>\r\n<li>117 Lotes Aeronáuticos</li>\r\n<li>188 Lotes Comerciais / Industriais</li>\r\n</ul>', 2, 1),
(6, '<h4>INFRAESTRUTURA COMPLETA</h4>\r\n\r\n<ul>\r\n<li>Hangaragem</li>\r\n<li>Centros de serviços</li>\r\n<li>Centros de treinamento de helicópteros e aeronaves</li>\r\n<li>Centro de convenções</li>\r\n<li>Hotel e restaurantes</li>\r\n<li>Centro comercial</li>\r\n<li>Escolas de aviação</li>\r\n</ul>', 3, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `usuario` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `senha` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `nome` varchar(100) CHARACTER SET utf16 COLLATE utf16_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id`, `usuario`, `senha`, `nome`) VALUES
(1, 'admin', 'admin123', 'Administrador');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `textos`
--
ALTER TABLE `textos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `textos`
--
ALTER TABLE `textos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
