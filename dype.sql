-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 01-Abr-2017 às 08:03
-- Versão do servidor: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dype`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `lotes`
--

CREATE TABLE `lotes` (
  `id` int(11) NOT NULL,
  `codigo` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `data` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `lotes`
--

INSERT INTO `lotes` (`id`, `codigo`, `data`) VALUES
(1, '00000001', '2017-04-01 00:00:00'),
(2, '00000002', '2017-04-01 00:00:00'),
(3, '00000003', '2017-04-01 00:00:00'),
(4, '00000004', '2017-04-01 00:00:00'),
(5, '00000005', '2017-04-01 00:00:00'),
(6, '00000006', '2017-04-01 00:00:00'),
(7, '00000007', '2017-04-01 00:00:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `lote_produto`
--

CREATE TABLE `lote_produto` (
  `id` int(11) NOT NULL,
  `id_lote` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `lote_produto`
--

INSERT INTO `lote_produto` (`id`, `id_lote`, `id_produto`, `quantidade`) VALUES
(1, 1, 1, 40),
(2, 1, 2, 40),
(3, 1, 7, 40),
(4, 1, 6, 30),
(5, 1, 12, 50),
(6, 1, 45, 100),
(7, 1, 13, 100),
(8, 2, 3, 15),
(9, 2, 17, 200),
(10, 2, 15, 100),
(11, 2, 16, 100),
(12, 2, 3, 15),
(13, 2, 3, 15),
(14, 2, 37, 120),
(15, 2, 42, 120),
(16, 2, 38, 120),
(17, 2, 10, 500),
(18, 3, 8, 80),
(19, 3, 17, 130),
(20, 3, 14, 200),
(21, 3, 13, 50),
(22, 3, 31, 50),
(23, 4, 27, 150),
(24, 4, 23, 150),
(25, 4, 43, 55),
(26, 4, 19, 45),
(27, 4, 40, 60),
(28, 4, 39, 60),
(29, 4, 35, 60),
(30, 5, 47, 45),
(31, 5, 46, 100),
(32, 5, 40, 25),
(33, 5, 39, 25),
(34, 5, 23, 30),
(35, 5, 9, 75),
(36, 6, 9, 50),
(37, 6, 10, 50),
(38, 6, 11, 50),
(39, 6, 15, 50),
(40, 6, 30, 50),
(41, 6, 8, 30),
(42, 7, 5, 35),
(43, 7, 4, 40),
(44, 7, 3, 30),
(45, 7, 1, 25),
(46, 7, 2, 25),
(47, 7, 37, 15),
(48, 7, 42, 15),
(49, 7, 38, 25),
(50, 7, 44, 20),
(51, 7, 36, 45),
(52, 7, 41, 20),
(53, 7, 20, 100),
(54, 7, 22, 40),
(55, 7, 21, 30),
(56, 7, 24, 30),
(57, 7, 25, 30),
(58, 7, 26, 30),
(59, 7, 35, 25),
(60, 7, 47, 25),
(61, 7, 43, 25),
(62, 7, 9, 12),
(63, 7, 10, 12),
(64, 7, 11, 12),
(65, 7, 30, 12),
(66, 7, 29, 12),
(67, 7, 30, 12);

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos`
--

CREATE TABLE `produtos` (
  `id` int(11) NOT NULL,
  `codigo` varchar(13) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `categoria` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `valor` double NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `produtos`
--

INSERT INTO `produtos` (`id`, `codigo`, `descricao`, `categoria`, `valor`) VALUES
(1, '0000000000001', 'Maçã', 'Frutas', 3.5),
(2, '0000000000002', 'Banana', 'Frutas', 2.5),
(3, '0000000000003', 'Mamão', 'Frutas', 4),
(4, '0000000000004', 'Uva', 'Frutas', 5),
(5, '0000000000005', 'Abacate', 'Frutas', 4.3),
(6, '0000000000006', 'Queijo', 'Laticínios‎', 5),
(7, '0000000000007', 'Leite', 'Laticínios‎', 3),
(8, '0000000000008', 'Iogurte', 'Laticínios‎', 2),
(9, '0000000000009', 'Água mineral', 'Bebidas', 5),
(10, '0000000000010', 'Refrigerante', 'Bebidas', 4),
(11, '0000000000011', 'Suco natural', 'Bebidas', 4),
(12, '0000000000012', 'Pão de forma', 'Cereais, pães e massas', 1.75),
(13, '0000000000013', 'Sucrilhos', 'Cereais, pães e massas', 8),
(14, '0000000000014', 'Barra de cereal', 'Cereais, pães e massas', 1.75),
(15, '0000000000015', 'Chocolate', 'Açúcares e doces', 7),
(16, '0000000000016', 'Alfajore', 'Açúcares e doces', 3),
(17, '0000000000017', 'Bolacha recehada', 'Açúcares e doces', 2),
(18, '0000000000018', 'Bala', 'Açúcares e doces', 0.25),
(19, '0000000000019', 'Azeite', 'Óleos e gorduras', 4),
(20, '0000000000020', 'Batata', 'Leguminosas', 1.54),
(21, '0000000000021', 'Cenoura', 'Leguminosas', 2),
(22, '0000000000022', 'Chuchu', 'Leguminosas', 1.5),
(23, '0000000000023', 'Feijão', 'Leguminosas', 3),
(24, '0000000000024', 'Brócolis', 'Hortaliças', 3),
(25, '0000000000025', 'Alface', 'Hortaliças', 0.55),
(26, '0000000000026', 'Espinafre', 'Hortaliças', 3),
(27, '0000000000027', 'Massa', 'Cereais, pães e massas', 2.5),
(28, '0000000000028', 'Pão', 'Cereais, pães e massas', 1.5),
(29, '0000000000029', 'Energético', 'Bebidas', 8),
(30, '0000000000030', 'Chá', 'Bebidas', 5),
(31, '0000000000031', 'Chocolate', 'Bebidas', 4),
(32, '0000000000032', 'Aveia', 'Cereais, pães e massas', 6),
(33, '0000000000400', 'Marshmallow', 'Açúcares e doces', 5.5),
(34, '0000000000600', 'Açúcar', 'Açúcares e doces', 2.22),
(35, '0000000000700', 'Cebolinha', 'Hortaliças', 0.5),
(36, '0000000000800', 'Hortelã', 'Hortaliças', 1),
(37, '0000000000111', 'Laranja', 'Frutas', 1.8),
(38, '0000000000222', 'Bergamota', 'Frutas', 1.3),
(39, '0000000000444', 'Cebola', 'Leguminosas', 0.8),
(40, '0000000000100', 'Tomate', 'Leguminosas', 1),
(41, '0000000000200', 'Couve-Flor', 'Leguminosas', 1),
(42, '0000000000333', 'Limão', 'Frutas', 1),
(43, '0000000000300', 'Ervilha', 'Leguminosas', 2),
(44, '0000000000900', 'Chicória', 'Hortaliças', 0.5),
(45, '0000000000555', 'Bolo', 'Cereais, pães e massas', 6.5),
(46, '0000000000888', 'Arroz', 'Cereais, pães e massas', 2.5),
(47, '0000000000999', 'Milho', 'Leguminosas', 2.1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `lotes`
--
ALTER TABLE `lotes`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `codigo` (`codigo`);

--
-- Indexes for table `lote_produto`
--
ALTER TABLE `lote_produto`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `produtos`
--
ALTER TABLE `produtos`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `codigo` (`codigo`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `lotes`
--
ALTER TABLE `lotes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `lote_produto`
--
ALTER TABLE `lote_produto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;
--
-- AUTO_INCREMENT for table `produtos`
--
ALTER TABLE `produtos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
